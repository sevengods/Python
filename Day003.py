"""
day2  data type
"""

a = 321
b = 123

print (a+b)
print (a-b)
print (a*b)
print (a/b)
print (a//b)
print (a%b)
print (a**b)
print (a-b-b)

# 使用input函数进行输入
# a = input ('a = ')
# print (a)
# 使用int函数进行类型转换
a = int(input ('a = '))
b = int(input ('b = '))
print ('%d + %d = %d' % (a,b,a+b))
print ('%d - %d = %d' % (a,b,a-b))
print ('%d * %d = %d' % (a,b,a*b))
print ('%d / %d = %d' % (a,b,a//b))
print ('%d // %d = %d' % (a,b,a//b))
print ('%d %% %d = %d' % (a,b,a%b))
print ('%d ** %d = %d' % (a,b,a**b))

# 使用type函数检查变量的类型 
a = 100
b = 7.77
c = 1+ 5j
d = 'zhouzhou'
e = True

print(type(a))
print(type(b))
print(type(c))
print(type(d))
print(type(e))

# int()将一个数值或字符串转换成整数，可以指定进制。
# float（）将一个字符串转换成浮点。
