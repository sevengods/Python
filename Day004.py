"""
python的流程控制
"""
value = float(input ('请输入长度：'))
unit = input ('请输入单位：')
if unit == 'in' or unit == '英寸':
    print ('%f英寸 = %f厘米' % (value,value * 2.54))
elif unit == 'cm' or unit == '厘米':
    print ('%f厘米 = %f英寸' % (value,value / 2.54))
else:
    print ('请输入有效的单位！')


"""
掷骰子
"""
from random import randint

face = randint (1,6)
if face == 1:
    result = '6'
elif face == 2:
    result ='5'
elif face == 3:
    result = '4'
elif face == 4:
    result = '3'
elif face == 5:
    result = '2'
else:
    result = '1'
print(result)

"""
判断输入的边长能否构成三角形
如果能则计算出三角形的周长和面积
"""

import math

a = float (input('a = '))
b = float (input('b = '))
c = float (input('c = '))


